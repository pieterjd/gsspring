# My getting started with Spring MVC after a long time

The goal is to create a MVC WebApp with the spring framework using Eclipse as IDE and Maven as build tool.

## Setup the project

Checkout [Crunchify.com blog post](http://crunchify.com/how-to-create-dynamic-web-project-using-maven-in-eclipse/)
Follow the steps. Pay attention to step 5 as there is advice on how to solve the `The superclass “javax.servlet.http.HttpServlet” was not found on the Java Build Path index.jsp /CrunchifyMavenTutorial/src/main/webapp` error

### Points for attention
1. Have a TOMCAT Server running
1. Make sure that 'Targeted Runtimes' is set to TOMCAT in the project properties
1. Rightclick the project and choose 'Run as ...' > 'Run on Server'. If everything is ok, then you should something like this ![HelloWorld Screenshot](./screenshots/HelloWorld.png)
1. add a java folder under src/main
1. add properties to maven pom.xml file
~~~~
<properties>
		<org.springframework-version>3.2.1.RELEASE</org.springframework-version>
		<org.aspectj-version>1.6.10</org.aspectj-version>
		<org.slf4j-version>1.6.6</org.slf4j-version>
	</properties>
  ~~~~
1. add following dependency elements to dependenies element of the maven pom.xml file
  ~~~~
  <dependency>
  			<groupId>org.springframework</groupId>
  			<artifactId>spring-context</artifactId>
  			<version>${org.springframework-version}</version>
  			<exclusions>
  				<!-- Exclude Commons Logging in favor of SLF4j -->
  				<exclusion>
  					<groupId>commons-logging</groupId>
  					<artifactId>commons-logging</artifactId>
  				</exclusion>
  			</exclusions>
  		</dependency>
  		<dependency>
  			<groupId>org.springframework</groupId>
  			<artifactId>spring-webmvc</artifactId>
  			<version>${org.springframework-version}</version>
  		</dependency>
  ~~~~
1. If Eclipse/maven complaints that artificats are missing, then right click project > Maven > update project. Check 'force Update of snapshots/releases' and click 'OK'
1. After adding the WebConfig class extending WebMvcConfigurerAdapter you might get a warning/error that a class is referenced that could not be found. Update the spring version in the pom.xml file to at least version 4. In this case, version 4.3.2.RELEASE resolved this issue
1. Added HomeController but exception `java.lang.ClassNotFoundException: org.apache.commons.logging.LogFactory`-- Add the Apache common logging to [Maven dependency](http://mvnrepository.com/artifact/commons-logging/commons-logging/1.2)
~~~~
<!-- https://mvnrepository.com/artifact/commons-logging/commons-logging -->
<dependency>
    <groupId>commons-logging</groupId>
    <artifactId>commons-logging</artifactId>
    <version>1.2</version>
</dependency>
~~~~
1. after adding home.jsp an error `Can not find the tag library descriptor for "http://java.sun.com/jsp/jstl/core"``, solved by adding maven dependency. **Don't forget to update the maven project!**
~~~~
<dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>jstl</artifactId>
    <version>1.2</version>
</dependency>
~~~~
1. When you want to run on server and you get `Publishing failed with multiple errors`, then do a refresh of the project, maven clean, maven build and maven install, and try again.


## Adding persistence

1. Add a interface with standard DAO methods in data package, such as
  * `(id)`
  * `getAll()`
  * ...
1. Add an implementation for this interface -> can be basic, eg return a hardcoded list with objects for the `getAll()` method
1. Add a DataConfig class to the config package. Define a method with the `@Bean` annotation that returns an object of the DAO implementation
1. Add a controller that contains a method to return the result of the `getAll()` method to the view. Define a constructor in the controller that takes an data interface as parameter and give it the annotation `@Autowired`
1. create the view JSP.
1. Added a detail page for the message. Changed the message overview page so it shows it preview of the message and a link to all the message details

**TIP** If your JSP expression are not evaluated and you get in your output something like `{$message.id}` instead of the actual value of id, then add this to the beginning of the JSP file
~~~~
<%@ page isELIgnored="false" %>
~~~~

## Adding ORM
Here we're going to add Hibernate with a mysql backend. So add maven dependencies for the mysql jdbc driver, apache database pool, hibernate and the hibernate part of spring.

```
<!-- https://mvnrepository.com/artifact/org.apache.commons/commons-dbcp2 -->
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-dbcp2</artifactId>
			<version>2.1.1</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/mysql/mysql-connector-java -->
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<version>5.1.38</version>
		</dependency>
    <!-- https://mvnrepository.com/artifact/org.hibernate/hibernate-core -->
<dependency>
    <groupId>org.hibernate</groupId>
    <artifactId>hibernate-core</artifactId>
    <version>5.2.2.Final</version>
</dependency>
<!-- https://mvnrepository.com/artifact/org.springframework/spring-orm -->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-orm</artifactId>
    <version>4.3.2.RELEASE</version>
</dependency>


```

### How does HibernateDAO get its connection to the database?
Before using hibernate, there was a dummy DAO, storing objects in a list. This was defined as a bean and autowired in the controller taking care of saving new objects.

To get Hibernate working, remove this bean. Because the repository is injected into the hibernate DAO, Spring starts looking for a session factory. This session factory is defined as a bean and is then used to inject into the hibernate DAO.

And finally don't forget the hibernate annotations in the data object classes.
## Adding Validation

Validation can be done by annotating the data model classes. Just use an implementation of the javax.validation library. The hibernate implementation is recommend because you can then use the `@Valid` annotation in your controllers.
```

<dependency>
    <groupId>org.hibernate</groupId>
    <artifactId>hibernate-validator</artifactId>
    <version>5.2.4.Final</version>
</dependency>

```

Don't forget to update the maven repository!!

Next use the spring form tags, use them to bind input elements to the model and you can use the tag ``<sf:errors> ``to show validation errors, for instance:
```
<sf:form method="post" commandName="message">
		<!-- path refers to the field -->
		Name: <sf:input path="sender"/><sf:errors path="sender"/><br/>
      Message: <sf:textarea path="message" rows="4" cols="50" /><sf:errors path="message"/><br/>
      <!-- submit button is not bind to the model-> makes sense :) -->
      <input type="submit" value="Add" />
	</sf:form>
```

## Adding Security

```
<!-- https://mvnrepository.com/artifact/org.springframework.security/spring-security-web -->
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-web</artifactId>
			<version>3.2.3.RELEASE</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.springframework.security/spring-security-config -->
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-config</artifactId>
			<version>3.2.3.RELEASE</version>
		</dependency>
```

Add files to your configuration
1. `SecurityWebInitializer extends AbstractSecurityWebApplicationInitializer` - No overrides necessary
1. `SecurityConfig extends WebSecurityConfigurerAdapter` (no overrides needed) with the `@Configuration` and `@EnableWebMvcSecurity` annotations
If you redeploy the webapp and visit it in a browser, you now get a login screenshot ![Visiting website results in redirect to Login Screenshot](./screenshots/loginScreen.png)
1. When overriding `configure(HttpSecurity http)` in SecurityConfig, ** do not call `super.configure(http)`** , it breaks things

## Adding rest service

dependencies to add conversion to json:
````
<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
			<version>2.7.5</version>
		</dependency>


````

Create new controller with `@RestController`annotation. Map url to methods as usual. The Spring in Action Book claims you need to define a Converter bean, but it the latest Spring version, this is not necesaary if jackson is in the classpath. :) :)

You might need to rebuild maven repository or do some refreshes. In this case, ther e were some problems downloading the jackson library

![Curl using rest service](./screenshots/curlScreenshot.png)

As for creating something new using POST, **DO NOT USE THE POSTMAN CHROME EXTENSION**. These calls return an HTTP 415 code. But if you use curl, everything works just fine.
```
@RequestMapping(value="messages",method=RequestMethod.POST)
	public @ResponseBody Message createMessage(@RequestBody Message message){
		System.out.println("trying to post using rest");
		messageRepository.save(message);
		return message;
	}
```

and the curl command is:
``
curl -XPOST -H'Content-Type: application/json' --data '{"sender":"restSend","message":"REST message from POST REST"}' http://localhost:8080/CrunchHelloWorld/api/v1/messages
``
## Adding static content/resources
If you want to add static resources such as css, js, images, ...
1. Add a resources folder at the same level as WEB-INF
1. Create subfolders if necessary
1. Override addRezourceHandlers in WebConfig
````
public void addResourceHandlers(ResourceHandlerRegistry registry) {
		// maps any reference to resources or subfolders in the jsps et all, to the folder resources in the webapp archive
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}
````

## Adding bootstrap
Fairly easy as you can use CDN for all the stylesheets & javascript

If you  want to include a custom version, the download it and put it under the resources folder (cf static resources section)
## Custom login page
If you want to pimp the login page ith bootstrap, you need a custom page.

1. Copy the source of the default login page and paste it in your new jsp, eg login.jsp
1. Change the code in SecurityConfig
````
protected void configure(HttpSecurity http) throws Exception {
		//super.configure(http);
		//by overriding this method, you need to add the formLogin explictly
		http
		.authorizeRequests()
		.antMatchers(HttpMethod.GET,"/addMessage").hasRole("ADMIN")
		.anyRequest().permitAll()
		.and()
		.formLogin().loginPage("/nice_login");
		;
````
1. Create a new Controller and map the `/nice_login`url to the jsp view. You now should get the look and feel of the default login screen, but now you can customize it

## Testing
Instead of creating beans manually, why not using the spring java config stuff? Use this snippet to get access to the sessionFactory, giving access to the database (in this example,
	all configuration is in the class `DataConfig.java`)

	```
	AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		ctx.register(DataConfig.class);
		ctx.refresh();
		//sessionFactory is the name of the bean I want
		System.out.println(ctx.getBean("sessionFactory"));
	```

## How to get logged in user

```
Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
System.out.println("Name loggedin user:" +authentication.getName());
```
## Feature request: add read status

* Do not use read as field name -> it is a reserved name is mysql, and cannot be used as a column name
* With POJOs, the getter of a boolean field needs to start with 'get' as well, otherwise this field will not be available is JSPs. Example: a getter for a valid field must be called `getValid()`, not `isValid()`

## MySQL Command line
Mysql poorly supported on mac, everything via cmd.
* add new user `GRANT ALL PRIVILEGES ON pieterjd.* To 'pieterjd'@'localhost' IDENTIFIED BY '<password>';`


## Angular errors and things to pay attention to
* When using dependencies, make sure the js files are included in the html file. Eg: js file depends on ngresource, but the html file that includes said js file did not include the resource script, hence causing the next error
```
angular.js:38Uncaught Error: [$injector:modulerr] http://errors.angularjs.org/1.4.3/$injector/modulerr?p0=demo&p1=Error%3A%20…ogleapis.com%2Fajax%2Flibs%2Fangularjs%2F1.4.3%2Fangular.min.js%3A19%3A339)
```
### Only use 1 controller 'object'
At first, I had following code:
```
<body>
	<div ng-controller="MsgCtrl" id="div1"></div>
	<div ng-controller="MsgCtrl" id="div2"></div>
</body>
```
`div2` changes data in the controller, `div1` displays the data using `ng-repeat`. When adding data, `div1` wasn't updated. For some reason, I thought that controllers were singletons, meaning both divs reference the same controller 'object'. **This is not true!**

If you want both divs to share the same data, then they must share the same controller object. As such, move the the controller one level up in the DOM.

```
<body ng-controller="MsgCtrl">
	<div id="div1"></div>
	<div id="div2"></div>
</body>
```
Now everything works as expected.
## Things to read / do
* ~~Rest service est. time: 1 to 2 hours~~
* [How to deploy maven to (remote) tomcat](http://www.mkyong.com/maven/how-to-deploy-maven-based-war-file-to-tomcat/)
* [Published with errors](http://stackoverflow.com/questions/5618652/publishing-failed-with-multiple-errors-eclipse)
